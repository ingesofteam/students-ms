FROM mcr.microsoft.com/java/jre:15-zulu-alpine
RUN mkdir /app
WORKDIR /app
COPY ./build/libs/students-ms-0.0.1-SNAPSHOT.jar /app/app.jar
ENTRYPOINT ["java","-jar","/app/app.jar"]