package edu.eam.ingesoft.students.config

object Groups {
    const val SYSTEM_ADMINISTRATOR = "systemAdministrator"
    const val PROGRAM_DIRECTOR = "programDirector"
    const val REGISTRATION_AND_CONTROL = "registrationAndControl"
    const val CLASS_TEACHER = "classTeacher"
}
