package edu.eam.ingesoft.students.controllers

import edu.eam.ingesoft.students.config.Groups
import edu.eam.ingesoft.students.config.Permissions
import edu.eam.ingesoft.students.config.Routes
import edu.eam.ingesoft.students.security.Secured
import edu.eam.ingesoft.students.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.ACADEMIC_PROGRAM_PATH)
class AcademicProgramController {
    @Autowired
    lateinit var studentService: StudentService

    @Secured(permissions = [Permissions.LIST_STUDENT_BY_ACADEMIC_PROGRAM], groups = [Groups.REGISTRATION_AND_CONTROL, Groups.SYSTEM_ADMINISTRATOR, Groups.CLASS_TEACHER, Groups.PROGRAM_DIRECTOR])
    @GetMapping(Routes.LIST_STUDENTS_BY_PROGRAM_PATH)
    fun findStudentByAcademicProgram(pageable: Pageable, @PathVariable("idProgram") idProgram: String) =
        studentService.findStudentsByProgramId(pageable, idProgram)
}
