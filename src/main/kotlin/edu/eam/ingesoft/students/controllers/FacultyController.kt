package edu.eam.ingesoft.students.controllers

import edu.eam.ingesoft.students.config.Routes
import edu.eam.ingesoft.students.services.StudentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(Routes.FACULTY_PATH)
class FacultyController {

    @Autowired
    lateinit var studentService: StudentService

    @GetMapping(Routes.FIND_STUDENTS_FACULTY_PATH)
    fun findStudentsByFacultyId(@PathVariable idFaculty: Long, pageable: Pageable) =
        studentService.findStudentsByFacultyId(idFaculty, pageable)
}
