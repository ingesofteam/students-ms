package edu.eam.ingesoft.students.externalapi.model.responses

data class AcademicProgramResponse(
    val id: Long,
    val name: String,
    val snies: String,
    val credits: Int,
    val faculty: FacultyResponse,
    val semesterQty: Int,
    var enabled: Boolean? = false
)
