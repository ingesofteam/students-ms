package edu.eam.ingesoft.students.externalapi.model.responses

data class FacultyResponse(

    val id: Long
)
