package edu.eam.ingesoft.students.externalapi.services

import edu.eam.ingesoft.students.externalapi.client.AcademicMsClient
import edu.eam.ingesoft.students.externalapi.model.responses.FacultyResponse
import edu.eam.ingesoft.students.externalapi.model.responses.ProgramResponse
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.EntityNotFoundException

@Component
class AcademicMsService {

    @Autowired
    lateinit var academicMsClient: AcademicMsClient

    fun findProgram(idProgram: Long): ProgramResponse {
        try {
            return academicMsClient.getProgram(idProgram)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("The program doesn't exist.")
        } catch (exc: FeignException) {
            throw Exception("Unexpected Exception.")
        }
    }

    fun getProgramByFacultyId(idFaculty: Long): List<edu.eam.ingesoft.students.externalapi.model.responses.AcademicProgramResponse> {

        try {
            return academicMsClient.getAllProgramsFromFaculty(idFaculty)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("Program does not exist")
        }
    }

    fun getFacultyById(idFaculty: Long): FacultyResponse {

        try {
            return academicMsClient.getFacultyById(idFaculty)
        } catch (exc: FeignException.NotFound) {
            throw EntityNotFoundException("The faculty does not exist")
        }
    }
}
