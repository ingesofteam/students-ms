package edu.eam.ingesoft.students.externalapi.services

import edu.eam.ingesoft.students.externalapi.client.AcademicMsClient
import edu.eam.ingesoft.students.externalapi.model.responses.AcademicProgramResponse
import feign.FeignException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.persistence.EntityNotFoundException

@Component
class AcademicProgramService {
    @Autowired
    lateinit var academicMsClient: AcademicMsClient
    fun getAcademicProgramById(id: String): AcademicProgramResponse {
        try {
            return academicMsClient.getAcademicProgram(id)
        } catch (exc: FeignException) {
            throw EntityNotFoundException("The program does not exist")
        }
    }
}
