package edu.eam.ingesoft.students.model.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "student")
data class Student(

    @Id
    @Column(name = "code")
    val code: String,

    @Column(name = "programId")
    val programId: String,

    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "identification_number")
    val person: Person,

    var enabled: Boolean? = true
)
