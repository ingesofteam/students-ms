package edu.eam.ingesoft.students.model.enums

enum class DocumentTypeEnum {
    CC, TI
}
