package edu.eam.ingesoft.students.model.enums

enum class GenderEnum {
    MALE, FEMALE
}
