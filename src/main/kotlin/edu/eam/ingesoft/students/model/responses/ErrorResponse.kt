package edu.eam.ingesoft.students.model.responses

data class ErrorResponse(
    val status: Int? = 500,
    val message: String? = "Exception"
)
