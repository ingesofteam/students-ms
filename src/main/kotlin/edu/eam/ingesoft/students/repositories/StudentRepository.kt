package edu.eam.ingesoft.students.repositories

import edu.eam.ingesoft.students.model.entities.Student
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface StudentRepository : PagingAndSortingRepository<Student, String> {
    fun findByProgramId(pageable: Pageable, programId: String): Page<Student>

    @Query("SELECT st FROM Student st WHERE st.programId IN :academicPrograms")
    fun findStudentsByAcademicProgramId(academicPrograms: List<String>, pageable: Pageable): Page<Student>
}
