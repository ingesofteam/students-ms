package edu.eam.ingesoft.students.security.authclient.model

data class Header(
    val name: String,
    val bearer: String
)
