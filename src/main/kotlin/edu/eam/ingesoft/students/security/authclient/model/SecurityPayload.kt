package edu.eam.ingesoft.students.security.authclient.model

data class SecurityPayload(
    val username: String,
    val permissions: List<String>,
    val groups: List<String>
)
