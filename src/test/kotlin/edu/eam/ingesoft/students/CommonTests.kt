import com.fasterxml.jackson.databind.ObjectMapper
import edu.eam.ingesoft.students.externalapi.client.AcademicMsClient
import edu.eam.ingesoft.students.repositories.PersonRepository
import edu.eam.ingesoft.students.repositories.StudentRepository
import edu.eam.ingesoft.students.security.authclient.SecurityClient
import edu.eam.ingesoft.students.security.authclient.model.Header
import edu.eam.ingesoft.students.security.authclient.model.SecurityPayload
import edu.eam.ingesoft.students.security.authclient.model.Token
import feign.FeignException
import feign.Request
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
@Transactional
abstract class CommonTests {

    @Autowired
    protected lateinit var objectMapper: ObjectMapper

    @Autowired
    protected lateinit var studentRepository: StudentRepository

    @Autowired
    protected lateinit var personRepository: PersonRepository

    @Autowired
    protected lateinit var mockMvc: MockMvc

    @MockBean
    protected lateinit var securityClient: SecurityClient

    @MockBean
    protected lateinit var academicMsClient: AcademicMsClient

    protected fun createFeignNotFoundException(body: String = "", method: Request.HttpMethod = Request.HttpMethod.POST) =
        FeignException.NotFound(
            "",
            Request.create(method, "null", emptyMap(), "".toByteArray(), null, null),
            body.toByteArray()
        )

    protected fun mockSecurity(
        tokenValue: String = "SecurityToken",
        permissions: List<String> = listOf(),
        groups: List<String> = listOf()
    ): Header {
        val token = Token(tokenValue)
        val payload = SecurityPayload(
            "username",
            permissions,
            groups
        )
        Mockito.`when`(securityClient.validateToken(token)).thenReturn(payload)

        return Header(name = "Authorization", bearer = "Bearer $tokenValue")
    }
}
