package edu.eam.ingesoft.students.controllers

import CommonTests
import edu.eam.ingesoft.students.config.Groups
import edu.eam.ingesoft.students.config.Permissions
import edu.eam.ingesoft.students.config.Routes
import edu.eam.ingesoft.students.externalapi.model.responses.AcademicProgramResponse
import edu.eam.ingesoft.students.externalapi.model.responses.FacultyResponse
import edu.eam.ingesoft.students.externalapi.model.responses.ProgramResponse
import edu.eam.ingesoft.students.model.entities.Person
import edu.eam.ingesoft.students.model.entities.Student
import edu.eam.ingesoft.students.model.enums.DocumentTypeEnum
import edu.eam.ingesoft.students.model.enums.GenderEnum
import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.Date

class StudentControllerTest : CommonTests() {

    @Test
    @Sql("/queries/createStudentWithPersonAlreadyCreatedTest.sql")
    fun createStudentWithPersonAlreadyCreatedTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_STUDENTS),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))

        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val students = studentRepository.findAll().toList()

        val studentToAssert = students.find { it.code == student.code }

        Assert.assertNotNull(studentToAssert)
        Assert.assertEquals(student.code, studentToAssert?.code)
    }

    @Test
    @Sql("/queries/createStudentWithPersonAlreadyCreatedTest.sql")
    fun createStudentUserWithoutPermissionTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("permission"),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))

        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    @Sql("/queries/createStudentWithPersonAlreadyCreatedTest.sql")
    fun createStudentUserWithoutHeaderTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    @Sql("/queries/createStudentWithPersonAlreadyCreatedTest.sql")
    fun createStudentUserWithWrongHeaderTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("permission"),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))

        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, "WrongToken")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }

    @Test
    fun createStudentWithoutPersonCreatedTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_STUDENTS),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))
        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isOk)

        val students = studentRepository.findAll().toList()

        val studentToAssert = students.find { it.code == student.code }

        val people = personRepository.findAll().toList()

        val personToAssert = people.find { it.identificationNumber == person.identificationNumber }

        Assert.assertNotNull(studentToAssert)
        Assert.assertEquals(student.code, studentToAssert?.code)

        Assert.assertNotNull(personToAssert)
        Assert.assertEquals(person.identificationNumber, personToAssert?.identificationNumber)
    }

    @Test
    @Sql("/queries/createStudentWithRepeatedEmailTest.sql")
    fun createStudentWithRepeatedEmailTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_STUDENTS),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))
        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Email is already in use.")))
    }

    @Test
    @Sql("/queries/createStudentWithRepeatedPhoneNumberTest.sql")
    fun createStudentWithRepeatedPhoneNumberTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_STUDENTS),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))
        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Phone number is already in use.")))
    }

    @Test
    @Sql("/queries/createStudentWithRepeatedCodeAndWithPersonCreatedTest.sql")
    fun createStudentWithRepeatedCodeAndWithPersonCreatedTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_STUDENTS),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))
        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Code is already in use.")))
    }

    @Test
    @Sql("/queries/createStudentWithRepeatedCodeAndWithoutPersonCreatedTest.sql")
    fun createStudentWithRepeatedCodeAndWithoutPersonCreatedTest() {
        val identificationNumber: String = "1005088761"
        val name: String = "Andres"
        val lastName: String = "Giraldo"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres@gmail.com"
        val phoneNumber: String = "3212345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "1"
        val enabledStu = true

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.CREATE_STUDENTS),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        Mockito.`when`(academicMsClient.getAcademicProgram(programId))
            .thenReturn(AcademicProgramResponse(1L, "Software", "202020", 85, FacultyResponse(1L), 9, true))
        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .post(Routes.STUDENTS_PATH)
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Code is already in use.")))
    }

    @Test
    @Sql("/queries/editStudentTest.sql")
    fun editStudentTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "lionel1@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val programId: String = "2"
        val enabledStu = true

        val student = Student(code, programId, person, enabledStu)

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val students = studentRepository.findAll().toList()

        val studentToAssert = students.find { it.code == student.code }

        Assert.assertNotNull(studentToAssert)
        Assert.assertEquals(student.programId, studentToAssert?.programId)
        Assert.assertEquals(student.person.phoneNumber, studentToAssert?.person?.phoneNumber)
        Assert.assertEquals(student.person.email, studentToAssert?.person?.email)
    }

    @Test
    @Sql("/queries/editStudentTest.sql")
    fun editStudentUserWithoutPermissionTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "lionel1@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val programId: String = "2"
        val enabledStu = true

        val student = Student(code, programId, person, enabledStu)

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("edit"),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    @Sql("/queries/editStudentTest.sql")
    fun editStudentUserWithoutHeaderTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "lionel1@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val programId: String = "2"
        val enabledStu = true

        val student = Student(code, programId, person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    @Sql("/queries/editStudentTest.sql")
    fun editStudentUserWithWrongHeaderTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "lionel1@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val programId: String = "2"
        val enabledStu = true

        val student = Student(code, programId, person, enabledStu)

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("edit"),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, "WrongToken")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }

    @Test
    fun editStudentDoesNotExistTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "lionel1@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val enabledStu = true

        val student = Student(code, program.id.toString(), person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/333")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student doesn't exist.")))
    }

    @Test
    @Sql("/queries/editStudentWithRepeatedEmailTest.sql")
    fun editStudentWithRepeatedEmailTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "messi@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val enabledStu = true

        val student = Student(code, program.id.toString(), person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Email is already in use.")))
    }

    @Test
    @Sql("/queries/editStudentWithRepeatedPhoneNumberTest.sql")
    fun editStudentWithRepeatedPhoneNumberTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "messi2001@gmail.com"
        val phoneNumber: String = "3182345890"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val enabledStu = true

        val student = Student(code, program.id.toString(), person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Phone number is already in use.")))
    }

    @Test
    @Sql("/queries/editStudentWithRepeatedCodeTest.sql")
    fun editStudentWithRepeatedCodeTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "messi2001@gmail.com"
        val phoneNumber: String = "3182345891"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val code: String = "333"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val enabledStu = true

        val student = Student(code, program.id.toString(), person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Code is already in use.")))
    }

    @Test
    @Sql("/queries/editStudentWithRepeatedIdentificationTest.sql")
    fun editStudentWithRepeatedIdentificationTest() {
        val identificationNumber: String = "1005088881"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "andres11@gmail.com"
        val phoneNumber: String = "3102382913"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val code: String = "123"
        val idProgram = 2L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")
        Mockito.`when`(academicMsClient.getProgram(program.id)).thenReturn(program)
        val enabledStu = true

        val student = Student(code, program.id.toString(), person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(412)))
            .andExpect(
                MockMvcResultMatchers.jsonPath(
                    "$.message",
                    Matchers.`is`("Identification number is already in use.")
                )
            )
    }

    @Test
    @Sql("/queries/editStudentProgramDoesntFoundInAcademicMsTest.sql")
    fun editStudentProgramDoesntFoundInAcademicMsTest() {
        val identificationNumber: String = "1005088762"
        val name: String = "Lionel"
        val lastName: String = "Messi"
        val gender: GenderEnum = GenderEnum.MALE
        val email: String = "lionel1@gmail.com"
        val phoneNumber: String = "3113414748"
        val birthDate: Date = Date()
        val documentType: DocumentTypeEnum = DocumentTypeEnum.CC
        val enabled: Boolean = true
        val person = Person(
            identificationNumber, name, lastName, gender,
            email, phoneNumber, birthDate, documentType, enabled
        )

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.EDIT_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL
            )
        )

        val code: String = "123"
        val idProgram = 0L
        val program: ProgramResponse = ProgramResponse(idProgram, "ProgramResponse Mockito")

        Mockito.`when`(academicMsClient.getProgram(program.id)).thenThrow(createFeignNotFoundException())
        val enabledStu = true

        val student = Student(code, program.id.toString(), person, enabledStu)

        val request = MockMvcRequestBuilders
            .put(Routes.STUDENTS_PATH + "/123")
            .content(objectMapper.writeValueAsString(student))
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(404)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("The program doesn't exist.")))
    }

    @Test
    @Sql("/queries/findStudentTest.sql")
    fun findStudentTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_STUDENT),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.PROGRAM_DIRECTOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .get(Routes.STUDENTS_PATH + "/123")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val students = studentRepository.findAll().toList()

        val studentToAssert = students.find { it.code == "123" }

        Assert.assertNotNull(studentToAssert)
        Assert.assertEquals("Lionel", studentToAssert?.person?.name)
        Assert.assertEquals("Messi", studentToAssert?.person?.lastName)
    }

    @Test
    @Sql("/queries/findStudentTest.sql")
    fun findStudentUserWithoutPermissionTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("permission"),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.PROGRAM_DIRECTOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )

        val request = MockMvcRequestBuilders
            .get(Routes.STUDENTS_PATH + "/123")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    @Sql("/queries/findStudentTest.sql")
    fun findStudentUserWithoutHeaderTest() {
        val request = MockMvcRequestBuilders
            .get(Routes.STUDENTS_PATH + "/123")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    @Sql("/queries/findStudentTest.sql")
    fun findStudentUserWithWrongHeaderTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_STUDENT),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.PROGRAM_DIRECTOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .get(Routes.STUDENTS_PATH + "/123")
            .header(header.name, "WrongToken")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }

    @Test
    @Sql("/queries/findStudentTest.sql")
    fun findStudentThatNotExistTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.FIND_STUDENT),
            groups = listOf(
                Groups.CLASS_TEACHER,
                Groups.PROGRAM_DIRECTOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .get(Routes.STUDENTS_PATH + "/325")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student not found.")))
    }

    @Test
    @Sql("/queries/activateStudentTest.sql")
    fun activateStudentTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val students = studentRepository.findAll().toList()

        val studentToAssert = students.find { it.code == "123" }

        Assert.assertNotNull(studentToAssert)
        Assert.assertEquals(true, studentToAssert?.enabled)
    }

    @Test
    @Sql("/queries/activateStudentTest.sql")
    fun activateStudentUserWithoutPermissionTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("Permissions"),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    @Sql("/queries/activateStudentTest.sql")
    fun activateStudentUserWithoutHeaderTest() {
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/activate")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    @Sql("/queries/activateStudentTest.sql")
    fun activateStudentUserWithWrongHeaderTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/activate")
            .header(header.name, "WrongToken")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }

    @Test
    fun activateStudentThatNotExistTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/301/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student doesn't exist.")))
    }

    @Test
    @Sql("/queries/activateStudentAlreadyActivatedTest.sql")
    fun activateStudentAlreadyActivatedTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.ACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/activate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student already activated.")))
    }

    @Test
    @Sql("/queries/deactivateStudentTest.sql")
    fun deactivateStudentTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DEACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)

        val students = studentRepository.findAll().toList()

        val studentToAssert = students.find { it.code == "123" }

        Assert.assertNotNull(studentToAssert)
        Assert.assertEquals(false, studentToAssert?.enabled)
    }

    @Test
    @Sql("/queries/deactivateStudentTest.sql")
    fun deactivateStudentUserWithoutPermissionTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("Permissions"),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    @Sql("/queries/deactivateStudentTest.sql")
    fun deactivateStudentUserWithoutHeaderTest() {
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/deactivate")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    @Sql("/queries/deactivateStudentTest.sql")
    fun deactivateStudentUserWithWrongHeaderTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("Permissions"),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/deactivate")
            .header(header.name, "WrongToken")

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }

    @Test
    fun deactivateStudentThatNotExistTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DEACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/301/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isNotFound)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student doesn't exist.")))
    }

    @Test
    @Sql("/queries/deactivateStudentAlreadyDeactivatedTest.sql")
    fun deactivateStudentAlreadyDeactivatedTest() {
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.DEACTIVATE_STUDENT),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR
            )
        )
        val request = MockMvcRequestBuilders
            .patch(Routes.STUDENTS_PATH + "/123/deactivate")
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isPreconditionFailed)
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("Student already deactivate.")))
    }

    @Test
    @Sql("/queries/findStudentsByFacultyId.sql")
    fun getStudentsByFacultyId() {

        val faculty = FacultyResponse(1)

        val academicPrograms = listOf<AcademicProgramResponse>(
            AcademicProgramResponse(1L, "Software", "202020", 85, faculty, 9, true),
            AcademicProgramResponse(2L, "Mecatronica", "303030", 95, faculty, 10, true)
        )

        Mockito.`when`(academicMsClient.getAllProgramsFromFaculty(1)).thenReturn(academicPrograms)

        val request = MockMvcRequestBuilders.get("${Routes.FACULTY_PATH}${Routes.FIND_STUDENTS_FACULTY_PATH}", 1L)
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(3)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].program_id", Matchers.`is`("1")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].program_id", Matchers.`is`("1")))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.page_number", Matchers.`is`(0)))
            .andExpect(MockMvcResultMatchers.jsonPath("size", Matchers.`is`(20)))
            .andExpect(MockMvcResultMatchers.jsonPath("sort.sorted", Matchers.`is`(false)))
    }

    @Test
    @Sql("/queries/findStudentsByProgramIdTest.sql")
    fun findStudentsByProgramIdTest() {
        val idProgram = "1"

        Mockito.`when`(academicMsClient.getProgram(idProgram.toLong())).thenReturn(ProgramResponse(1L, "Nombre"))

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.LIST_STUDENT_BY_ACADEMIC_PROGRAM),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.PROGRAM_DIRECTOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.CLASS_TEACHER
            )
        )

        val request = MockMvcRequestBuilders
            .get("${Routes.ACADEMIC_PROGRAM_PATH}${Routes.LIST_STUDENTS_BY_PROGRAM_PATH}?size=3", idProgram)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)

        response.andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("content", Matchers.hasSize<Int>(3)))
            .andExpect(MockMvcResultMatchers.jsonPath("content[0].program_id", Matchers.`is`("1")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[1].program_id", Matchers.`is`("1")))
            .andExpect(MockMvcResultMatchers.jsonPath("content[2].program_id", Matchers.`is`("1")))
            .andExpect(MockMvcResultMatchers.jsonPath("pageable.sort.sorted", Matchers.`is`(false)))
    }

    @Test
    @Sql("/queries/findStudentsByProgramIdTest.sql")
    fun findStudentsByProgramIdUserWithoutPermissionTest() {
        val idProgram = "1"

        Mockito.`when`(academicMsClient.getProgram(idProgram.toLong())).thenReturn(ProgramResponse(1L, "Nombre"))
        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf("Permissions"),
            groups = listOf(
                Groups.REGISTRATION_AND_CONTROL,
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.CLASS_TEACHER,
                Groups.PROGRAM_DIRECTOR
            )
        )
        val request = MockMvcRequestBuilders
            .get("${Routes.ACADEMIC_PROGRAM_PATH}${Routes.LIST_STUDENTS_BY_PROGRAM_PATH}?size=3", idProgram)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, header.bearer)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("this user has no access")))
    }

    @Test
    @Sql("/queries/findStudentsByProgramIdTest.sql")
    fun findStudentsByProgramIdUserWithoutHeaderTest() {
        val idProgram = "1"

        val request = MockMvcRequestBuilders
            .get("${Routes.ACADEMIC_PROGRAM_PATH}${Routes.LIST_STUDENTS_BY_PROGRAM_PATH}?size=3", idProgram)
            .contentType(MediaType.APPLICATION_JSON)

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("token required")))
    }

    @Test
    @Sql("/queries/findStudentsByProgramIdTest.sql")
    fun findStudentsByProgramIdUserWithWrongHeaderTest() {
        val idProgram = "1"

        Mockito.`when`(academicMsClient.getProgram(idProgram.toLong())).thenReturn(ProgramResponse(1L, "Nombre"))

        val header = mockSecurity(
            tokenValue = "Token",
            permissions = listOf(Permissions.LIST_STUDENT_BY_ACADEMIC_PROGRAM),
            groups = listOf(
                Groups.SYSTEM_ADMINISTRATOR,
                Groups.PROGRAM_DIRECTOR,
                Groups.REGISTRATION_AND_CONTROL,
                Groups.CLASS_TEACHER
            )
        )
        val request = MockMvcRequestBuilders
            .get("${Routes.ACADEMIC_PROGRAM_PATH}${Routes.LIST_STUDENTS_BY_PROGRAM_PATH}?size=3", idProgram)
            .contentType(MediaType.APPLICATION_JSON)
            .header(header.name, "WrongToken")

        val response = mockMvc.perform(request)
        response.andExpect(MockMvcResultMatchers.status().isForbidden)
            .andExpect(MockMvcResultMatchers.jsonPath("$.status", Matchers.`is`(403)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.`is`("invalid token")))
    }
}
