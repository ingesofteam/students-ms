INSERT INTO public.person
(identification_number, birth_date, document_type, email, enabled, gender, last_name, "name", phone_number)
VALUES('1005088762', '2001-06-01', 'CC', 'andres11@gmail.com', true, 'MALE', 'Messi', 'Lionel', '3102382913');

INSERT INTO public.person
(identification_number, birth_date, document_type, email, enabled, gender, last_name, "name", phone_number)
VALUES('1035082762', '2001-06-01', 'CC', 'andres21@gmail.com', true, 'MALE', 'Messi', 'Andres', '3102382111');

INSERT INTO public.person
(identification_number, birth_date, document_type, email, enabled, gender, last_name, "name", phone_number)
VALUES('1025019762', '2001-06-01', 'CC', 'andresito@gmail.com', true, 'MALE', 'Messi', 'Camilo', '3102322913');

INSERT INTO public.student
(code, enabled, program_id, person_id)
VALUES('123', true, '1', '1005088762');

INSERT INTO public.student
(code, enabled, program_id, person_id)
VALUES('124', true, '1', '1035082762');

INSERT INTO public.student
(code, enabled, program_id, person_id)
VALUES('125', true, '1', '1025019762');